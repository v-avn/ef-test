<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Bulletin;
use App\Repositories\BulletinRepository;

class BulletinController extends Controller
{
    /**
     * The bulletin repository instance.
     *
     * @var BulletinRepository
     */
    protected $bulletins;

    /**
     * Create a new controller instance.
     *
     * @param  BulletinRepository  $bulletins
     * @return void
     */
    public function __construct(BulletinRepository $bulletins)
    {
        $this->middleware('auth');

        $this->bulletins = $bulletins;
    }

    /**
     * Display a list of all bulletins.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view('bulletins.index', [
            'bulletins' => $this->bulletins->allActive()
        ]);
    }

    /**
     * Display a list of all of the user's bulletin.
     *
     * @param  Request  $request
     * @return Response
     */
    public function my(Request $request)
    {
        return view('bulletins.my', [
            'bulletins' => $this->bulletins->forUser($request->user())
        ]);
    }

    /**
     * Display a list of all of the user's offers.
     *
     * @param  Request  $request
     * @return Response
     */
    public function offers(Request $request)
    {
        return view('bulletins.offers', [
            'bulletins' => $this->bulletins->withUserOffer($request->user())
        ]);
    }

    /**
     * Create a new bulletin.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|max:255'
        ]);

        $img = '';
        if ($file = $request->file('img')) {
            $img = sha1_file($file->getRealPath()) . '.' . $file->getClientOriginalExtension();
            $file->move(storage_path('app/public'), $img);
            $img = '/storage/app/public/'.$img;
        }

        $request->user()->bulletins()->create([
            'title'  => $request->title,
            'text'   => $request->text,
            'img'    => $img,
            'price'  => $request->price,
            'status' => Bulletin::STATUSES['active']
        ]);

        return redirect('/bulletins');
    }
}
