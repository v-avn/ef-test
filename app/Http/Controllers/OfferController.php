<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Bulletin;
use App\Offer;

class OfferController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Create a new offer.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $user_id = $request->user()->id;
        $bulletin = Bulletin::where('user_id', '!=', $user_id)
            ->where('status', Bulletin::STATUSES['active'])
            ->where('id', $request->bulletin_id)
            ->first();

        if ($bulletin) {
            $offer = $bulletin->offers()->where('user_id', $user_id)->count();
            if (!$offer) {
                $this->validate($request, [
                    'title' => 'required|max:255',
                ]);
                $bulletin->offers()->create([
                    'user_id' => $user_id,
                    'title'   => $request->title,
                    'text'    => $request->text,
                    'price'   => $request->price,
                    'status'  => Offer::STATUSES['active']
                ]);
            }
        }

        return redirect('/bulletins');
    }

    /**
     * Check offer for bulletin.
     *
     * @param  Request  $request
     * @param  Offer    $offer
     * @return Response
     */
    public function check(Request $request, Offer $offer)
    {

        if ($offer->bulletin->user_id == $request->user()->id) {
            $offer->bulletin()
                ->update(['status' => Bulletin::STATUSES['completed']]);
            $offer->bulletin->offers()
                ->where('id', '!=', $offer->id)
                ->update(['status' => Offer::STATUSES['deleted']]);
        }

        return redirect('/bulletins');
    }
}
