<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('welcome');
    })->middleware('guest');

    Route::get('/bulletins', 'BulletinController@index');
    Route::get('/bulletins/my', 'BulletinController@my');
    Route::get('/bulletins/offers', 'BulletinController@offers');
    Route::post('/bulletin', 'BulletinController@store');
    Route::delete('/bulletin/{bulletin}', 'bulletinController@destroy');

    Route::post('/offer', 'OfferController@store');
    Route::post('/offer/{offer}/check', 'OfferController@check');

    Route::auth();

});
