<?php

namespace App\Repositories;

use App\User;
use App\Bulletin;

class BulletinRepository
{
    /**
     * Get all of the active bulletins.
     *
     * @return Collection
     */
    public function allActive()
    {
        return Bulletin::where('status', Bulletin::STATUSES['active'])
            ->orderBy('created_at', 'asc')
            ->get();
    }

    /**
     * Get all of the bulletins for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function forUser(User $user)
    {
        return Bulletin::where('user_id', $user->id)
            ->orderBy('created_at', 'asc')
            ->get();
    }

    /**
     * Get all of the bulletins for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function withUserOffer(User $user)
    {
        return Bulletin::whereHas('offers', function($query) use ($user) {
            $query->where('user_id', $user->id);
        })
            ->where('status', Bulletin::STATUSES['active'])
            ->orderBy('created_at', 'asc')
            ->get();
    }
}
