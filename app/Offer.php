<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    const STATUSES = [
        'active'  => 1,
        'deleted' => 0
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'title', 'text', 'price', 'status'];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'int',
        'bulletin_id' => 'int',
    ];

    /**
     * Get the user that owns the offer.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the bulletin that owns the offer.
     */
    public function bulletin()
    {
        return $this->belongsTo(Bulletin::class);
    }
}
