<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bulletin extends Model
{
    const STATUSES = [
        'active'    => 1,
        'completed' => 2
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'text', 'img', 'price', 'status'];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'int',
    ];

    /**
     * Get the user that owns the bulletin.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get all offers that owns the bulletin.
     */
    public function offers()
    {
        return $this->hasMany('App\Offer');
    }
}
