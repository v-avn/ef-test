@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">

            <!-- Current Bulletins -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    My Bulletins
                    <a class="btn btn-default btn-xs pull-right" href="#" role="button" data-toggle="modal" data-target="#bulletin">
                        <i class="fa fa-btn fa-plus"></i>Add Bulletin
                    </a>
                </div>

                <div class="panel-body">

                    <div class="row">
                        @foreach ($bulletins as $bulletin)
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="{{ $bulletin->img }}" >
                                <div class="caption">
                                    <h3>{{ $bulletin->title }}</h3>
                                    <p>{{ $bulletin->text }}</p>
                                    <pre style="text-align: right;">{{ $bulletin->price }}</pre>
                                    @if (count($bulletin->offers) > 0)
                                    <table class="table table-striped task-table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">
                                                    {{ $bulletin->status == $bulletin::STATUSES['active'] ? 'Offers For You' : 'You Checked Offer' }}
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($bulletin->offers as $offer)
                                            @if ($offer->status == $offer::STATUSES['active'])
                                            <tr>
                                                <td>
                                                    {{ $offer->title }}
                                                    <br>
                                                    <small>{{ $offer->text }}</small>
                                                </td>
                                                <td style="text-align: right;">
                                                    <b>{{ $offer->price }}</b>
                                                    @if ($bulletin->status == $bulletin::STATUSES['active'])
                                                    <br>
                                                    <form action="{{url('offer/' . $offer->id . '/check')}}" method="POST">
                                                        {{ csrf_field() }}
                                                        <button type="submit" id="check-offer-{{ $offer->id }}" class="btn btn-success btn-xs">
                                                            <i class="fa fa-btn fa-check"></i>Check
                                                        </button>
                                                    </form>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bulletin -->
    <div class="modal fade" id="bulletin" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <form action="{{ url('bulletin') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">New Bulletin</h4>
                    </div>

                    <div class="modal-body">

                        <div class="panel-body">
                            <!-- Display Validation Errors -->
                            @include('common.errors')

                            {{ csrf_field() }}

                            <!-- Bulletin Title -->
                            <div class="form-group">
                                <label for="bulletin-title" class="col-sm-3 control-label">Title</label>
                                <div class="col-sm-6">
                                    <input type="text" name="title" id="bulletin-title" class="form-control" value="">
                                </div>
                            </div>

                            <!-- Bulletin Text -->
                            <div class="form-group">
                                <label for="bulletin-text" class="col-sm-3 control-label">Text</label>
                                <div class="col-sm-6">
                                    <textarea name="text" id="bulletin-text" class="form-control"></textarea>
                                </div>
                            </div>

                            <!-- Bulletin Image -->
                            <div class="form-group">
                                <label for="bulletin-img" class="col-sm-3 control-label">Image</label>
                                <div class="col-sm-6">
                                    <input type="file" name="img" id="bulletin-img" class="form-control" value="">
                                </div>
                            </div>

                            <!-- Bulletin Price -->
                            <div class="form-group">
                                <label for="bulletin-title" class="col-sm-3 control-label">Price</label>

                                <div class="col-sm-6">
                                    <input type="number" name="price" id="bulletin-price" class="form-control" value="">
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-btn fa-plus"></i>Add Bulletin</button>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection